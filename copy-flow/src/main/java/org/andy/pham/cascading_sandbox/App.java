package org.andy.pham.cascading_sandbox;

import java.util.Properties;

import org.apache.log4j.Logger;

import cascading.flow.Flow;
import cascading.flow.FlowDef;
import cascading.flow.local.LocalFlowConnector;
import cascading.pipe.Pipe;
import cascading.property.AppProps;
import cascading.scheme.local.TextDelimited;
import cascading.tap.Tap;
import cascading.tap.local.FileTap;

public class App {

	private static Logger logger = Logger.getLogger(App.class);

	public static void main(String[] args) {
		
		String input = args[0];
		String output = args[1];

		//Source file is a CSV
		Tap inTap = new FileTap(new TextDelimited(false, ","), input);
		
		//Outputs a TSV file
		Tap outTap = new FileTap(new TextDelimited(), output);

		Pipe copyPipe = new Pipe("Copy file");

		FlowDef flowDef = FlowDef.flowDef().addSource(copyPipe, inTap).setName("Copy").addTailSink(copyPipe, outTap);

		Properties properties = AppProps.appProps().setName("copy_file").buildProperties();
		LocalFlowConnector flowConnector = new LocalFlowConnector(properties);

		Flow flow = flowConnector.connect(flowDef);

		flow.complete();
		logger.info("Copy completed!");
	}
}
