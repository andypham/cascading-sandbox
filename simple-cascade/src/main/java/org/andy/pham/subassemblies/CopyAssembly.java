package org.andy.pham.subassemblies;

import cascading.pipe.Pipe;
import cascading.pipe.SubAssembly;

public class CopyAssembly extends SubAssembly {

	public CopyAssembly() {

		setPrevious();

		Pipe copyPipe = new Pipe("copyPipe");

		setTails(copyPipe);

	}

}