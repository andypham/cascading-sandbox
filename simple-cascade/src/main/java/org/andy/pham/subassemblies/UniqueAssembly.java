package org.andy.pham.subassemblies;

import cascading.flow.Flow;
import cascading.pipe.Pipe;
import cascading.pipe.SubAssembly;
import cascading.pipe.assembly.Unique;
import cascading.tuple.Fields;

public class UniqueAssembly extends SubAssembly {

	Flow flow;

	public UniqueAssembly(Pipe pipe, Fields filterField) {

		setPrevious(pipe);

		Pipe uniquePipe = new Pipe("uniquePipe");

		uniquePipe = new Unique(uniquePipe, filterField);

		setTails(uniquePipe);

	}

	public Flow getFlow() {
		return flow;
	}

}
