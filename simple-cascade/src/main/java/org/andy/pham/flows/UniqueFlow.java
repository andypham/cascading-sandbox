package org.andy.pham.flows;

import java.util.Properties;

import org.andy.pham.subassemblies.UniqueAssembly;

import cascading.flow.Flow;
import cascading.flow.FlowDef;
import cascading.flow.local.LocalFlowConnector;
import cascading.pipe.Pipe;
import cascading.property.AppProps;
import cascading.scheme.local.TextDelimited;
import cascading.tap.SinkMode;
import cascading.tap.Tap;
import cascading.tap.local.FileTap;
import cascading.tuple.Fields;

public class UniqueFlow {

	Flow flow;

	public UniqueFlow(String input, String output, Pipe pipe) {

		Fields apacheFields = new Fields("name", "age", "gender").applyTypes(
				String.class, long.class, String.class);

		Tap inTap = new FileTap(new TextDelimited(apacheFields, "\t"), input);
		Tap outTap = new FileTap(new TextDelimited(true, "\t"), output,
				SinkMode.REPLACE);
		
		UniqueAssembly uniquePipe = new UniqueAssembly(pipe, new Fields("name"));

		FlowDef flowDef = FlowDef.flowDef().addSource(uniquePipe, inTap)
				.setName("Unique").addTailSink(uniquePipe, outTap);

		Properties properties = AppProps.appProps().setName("unique_file")
				.buildProperties();
		LocalFlowConnector flowConnector = new LocalFlowConnector(properties);

		this.flow = flowConnector.connect(flowDef);

	}

	public Flow getFlow() {
		return flow;
	}
}
