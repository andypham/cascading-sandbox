package org.andy.pham.flows;

import java.util.Properties;

import org.andy.pham.subassemblies.CopyAssembly;

import cascading.flow.Flow;
import cascading.flow.FlowDef;
import cascading.flow.local.LocalFlowConnector;
import cascading.property.AppProps;
import cascading.scheme.local.TextDelimited;
import cascading.tap.Tap;
import cascading.tap.local.FileTap;

public class CopyFlow {

	String output;
	CopyAssembly copyPipe;
	Flow flow;

	public CopyFlow(String input, String output) {

		this.output = output;

		Tap inTap = new FileTap(new TextDelimited(false, ","), input);
		Tap outTap = new FileTap(new TextDelimited(), output);

		this.copyPipe = new CopyAssembly();

		FlowDef flowDef = FlowDef.flowDef().addSource(copyPipe, inTap)
				.setName("Copy").addTailSink(copyPipe, outTap);

		Properties properties = AppProps.appProps().setName("copy_file")
				.buildProperties();
		LocalFlowConnector flowConnector = new LocalFlowConnector(properties);

		this.flow = flowConnector.connect(flowDef);
	}

	public String getOutput() {
		return output;
	}
	
	public CopyAssembly getPipe() {
		return copyPipe;
	}
	
	public Flow getFlow() {
		return flow;
	}

}
