package org.andy.pham.cascading_sandbox;

import org.andy.pham.flows.CopyFlow;
import org.andy.pham.flows.UniqueFlow;
import org.apache.log4j.Logger;

import cascading.cascade.Cascade;
import cascading.cascade.CascadeConnector;
import cascading.cascade.CascadeDef;

public class App {

	private static Logger logger = Logger.getLogger(App.class);

	public static void main(String[] args) {

		String input = args[0]; //CSV file with name, age, gender and no headers
		String output = args[1]; //TSV file with headers and name (unique), age, gender

		/* Flow 1 */
		
		CopyFlow copyFlow = new CopyFlow(input,"output/copy_output.tsv");

		/* Flow 2 */
		
		UniqueFlow uniqueFlow = new UniqueFlow(copyFlow.getOutput(), output, copyFlow.getPipe());

		/* Cascading Flow 1 and 2 */
		
		CascadeConnector connector = new CascadeConnector();

		CascadeDef def = CascadeDef.cascadeDef().addFlow(copyFlow.getFlow())
				.addFlow(uniqueFlow.getFlow());

		Cascade cascade = connector.connect(def);

		cascade.complete();
		logger.info("Cascade complete!");

	}
}
